## Deploy the Monax network

```
monax init
monax chains make govpurchase --unsafe
monax chains start govpurchase --init-dir ~/.monax/chains/govpurchase/govpurchase_full_000/
```

## Deploy a contract

`npm run deploy`

