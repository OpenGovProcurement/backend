pragma solidity ^0.4.10;
contract GovPurchaseContract {

    struct Offer {
        string offer_data;
    }

    struct Purchase {
        string purchase_data;
        mapping (uint => Offer) offers;
        uint offers_length;
        uint64 accepted_offer;
        bool accepted;
    }

    Purchase[] purchases;

    function getOffersCount(uint64 purchase_id) constant returns (uint256, bool) {
        if (purchase_id >= purchases.length)
            return (0, false);

        return (purchases[purchase_id].offers_length, true);
    }

    function getPurchasesCount() constant returns (uint256) {
        return purchases.length;
    }

    function getOffer(uint64 purchase_id, uint64 offer_number) constant returns (string, uint256, bool) {
        if (purchase_id >= purchases.length)
            return ("", 0, false);

        if (offer_number >= purchases[purchase_id].offers_length)
            return ("", 0, false);

        return (purchases[purchase_id].offers[offer_number].offer_data, purchases[purchase_id].offers_length, true);
    }

    function getPurchase(uint64 purchase_id) constant returns (string, bool, uint64, bool) {
        if (purchase_id >= purchases.length)
            return ("", false, 0, false);
        return (purchases[purchase_id].purchase_data, purchases[purchase_id].accepted, purchases[purchase_id].accepted_offer, true);
    }

    function addPurchase(string purchase_data) returns (uint256) {
        Purchase memory purchase;
        purchase.purchase_data = purchase_data;
        purchase.accepted = false;
        purchases.push(purchase);
        return purchases.length - 1;
    }

    function addOffer(string offer_data, uint64 purchase_id) returns (uint256, bool) {
        if (purchase_id >= purchases.length)
            return (0, false);

        Offer memory offer;
        offer.offer_data = offer_data;
        purchases[purchase_id].offers[purchases[purchase_id].offers_length] = offer;
        purchases[purchase_id].offers_length += 1;
        return (purchases[purchase_id].offers_length - 1, true);
    }

    function acceptOffer(uint64 purchase_id, uint64 offer_id) returns (string, bool) {
        if (purchases[purchase_id].offers_length < offer_id) {
            purchases[purchase_id].accepted_offer = offer_id;
            purchases[purchase_id].accepted = true;
            return (purchases[purchase_id].offers[offer_id].offer_data, true);
        } else {
            return ("", false);
        }
    }
}