const contractApiWrapper = require('./');

console.log(contractApiWrapper.contractInfo);

contractApiWrapper.getContractFactory().new({ data: contractApiWrapper.contractInfo.bin }, function (err, contract) {
    if (err) {
        console.error(err);
        return err;
    }

    console.log(contract);
});