const path = require('path');
const config = require('../config');
const promisify = require('es6-promisify');

const fs = require('fs');
const api = require('@monax/legacy-contracts');

class GovPurchaseSmartContract {
    constructor(contract) {
        this.contract = contract;
    }

    getPurchasesCount() {
        return new Promise((resolve, reject) => {
            this.contract.getPurchasesCount((err, result) => {
                return err ? reject(err) : resolve(result.toNumber());
            });
        });
    }

    getOffersCount(purchaseId) {
        return new Promise((resolve, reject) => {
            this.contract.getOffersCount(purchaseId, (err, result) => {
                return err || result[1] == false ? reject(err || 'invalid params') : resolve(result[0].toNumber());
            });
        });
    }

    getPurchase(purchaseId) {
        return new Promise((resolve, reject) => {
            this.contract.getPurchase(purchaseId, (err, [data, accepted, acceptedOfferId, error]) => {
                const obj = JSON.parse(data);
                obj.accepted = accepted;
                obj.acceptedOfferId = acceptedOfferId;

                return err || error == false ? reject(err || 'invalid params') : resolve(obj);
            });
        });
    }

    addPurchase(obj) {
        return new Promise((resolve, reject) => {
            this.contract.addPurchase(JSON.stringify(obj), (err, result) => {
                return err ? reject(err) : resolve(result.toNumber());
            });
        });
    }

    getOffer(purchaseId, offerId) {
        return new Promise((resolve, reject) => {
            this.contract.getOffer(purchaseId, offerId, (err, result) => {
                return err || result[2] == false ? reject(err || 'invalid params') : resolve(result[0].toNumber());
            });
        });
    }

    addOffer(purchaseId, obj) {
        return new Promise((resolve, reject) => {
            this.contract.addOffer(purchaseId, JSON.stringify(obj), (err, result) => {
                return err || result[1] == false ? reject(err || 'invalid params') : resolve(result[0].toNumber());
            });
        });
    }

    acceptOffer(purchaseId, offerId) {
        return new Promise((resolve, reject) => {
            this.contract.acceptOffer(purchaseId, offerId, (err, [offerObj, error]) => {
                return err || error == false ? reject(err || 'invalid params') : resolve(JSON.parse(offerObj));
            });
        });
    }
}

const contractInfo = {
    abi: JSON.parse(fs.readFileSync(path.resolve(path.dirname(__filename), 'contract_sol_GovPurchaseContract.abi'))),
    bin: fs.readFileSync(path.resolve(path.dirname(__filename), 'contract_sol_GovPurchaseContract.bin'))
};

function getContractFactory() {
    const myContractManager = api.newContractManagerDev(config.apiEndpoint, config.account);
    var myContractFactory = myContractManager.newContractFactory(contractInfo.abi);
    return myContractFactory;
}

function getContractInstance(address, callback) {
    const myContractFactory = getContractFactory();
    return callback ? myContractFactory.at(address, callback) : promisify(myContractFactory.at)(address);
}

function getSmartContract(address) {
    return new Promise((resolve, reject) => {
        getContractInstance(address, (err, contract) => {
            return err ? reject(err) : resolve(new GovPurchaseSmartContract(contract));
        });
    });
}

module.exports = {
    contractInfo: contractInfo,
    getContractFactory: getContractFactory,
    getContractInstance: getContractInstance,
    getSmartContract: getSmartContract
};