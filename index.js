const config = require('./config');
const app = require('./server');
const logger = require('./lib/logger')('index');

app.listen(config.server.port, config.server.host);
logger.info('listening on %s:%d', config.server.host, config.server.port);
