const winston = require('winston');
const config = require('../config');

const logFile = config.logFile;


module.exports = function (name) {
    return new winston.Logger({
        transports: [
            new (winston.transports.Console)({
                level: 'debug',
                prettyPrint: JSON.stringify,
                colorize: true,
                silent: false,
                timestamp: true,
                label: name
            }),
            new (winston.transports.File)({ filename: logFile, label: name })
        ]
    });
};
