const express = require('express');
const logger = require('./logger')('rest');

function sendJson(res, obj, code=200) {
    const content = JSON.stringify(obj);
    logger.debug('sending %s', content);
    res.status(code).set('Content-Type', 'application/json').send(content);
}

function createRestApi(contract) {
    const router = express.Router();

    router.use((req, res, next) => {
        logger.info('processing %s, method %s', req.originalUrl, req.method);

        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });

    router.get('/purchase', (req, res) => {
        var limit = parseInt(req.query.limit || Number.MAX_VALUE);
        contract.getPurchasesCount().then(async (obj) => {
            var purchases = [];

            for (var id = 0; id < Math.min(limit, obj); id++) {
                try {
                    const obj = await contract.getPurchase(id);
                    obj.id = id;
                    purchases.push(obj);
                } catch (e) {
                    logger.error(e);
                }
            }

            sendJson(res, purchases);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.get('/purchase/:id', (req, res) => {
        const id = parseInt(req.params.id);

        contract.getPurchase(id).then(obj => {
            sendJson(res, obj);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.post('/purchase', (req, res) => {
        contract.addPurchase(req.body).then(data => {
            sendJson(res, data);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.get('/offer/:purchaseId/:offerId', (req, res) => {
        const purchaseId = parseInt(req.params.purchaseId);
        const offerId = parseInt(req.params.offerId);

        contract.getOffer(purchaseId, offerId).then(obj => {
            sendJson(res, obj);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.get('/offer/:purchaseId', (req, res) => {
        const purchaseId = parseInt(req.params.purchaseId);
        contract.getOffersCount(purchaseId).then(async (obj) => {
            var offers = [];
            for (var offerId = 0; offerId < obj; offerId++) {
                const offer = await contract.getOffer(purchaseId, offerId);
                offer.id = offerId;
                offers.push(offer);
            }

            sendJson(res, offers);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.post('/offer/:purchaseId', (req, res) => {
        const purchaseId = parseInt(req.params.purchaseId);

        contract.addOffer(purchaseId, req.body).then(obj => {
            sendJson(res, obj);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    router.post('/offer/:purchaseId/:offerId', (req, res) => {
        const purchaseId = parseInt(req.params.purchaseId);
        const offerId = parseInt(req.params.offerId);

        contract.acceptOffer(purchaseId, offerId).then(obj => {
            sendJson(res, obj);
        }).catch(err => {
            sendJson(res, {error: err}, 400);
        });
    });

    return router;
}

module.exports = createRestApi;