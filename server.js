// server.js

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config');
const logger = require('./lib/logger')('server');
const path = require('path');
const rest = require('./lib/rest');
const contractWrapper = require('./contract');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.disable('etag');

app.use('/', express.static('static'));
logger.info('file storage is %s', path.resolve(config.fileStorage.path));
app.use('/storage', express.static(config.fileStorage.path));


contractWrapper.getSmartContract(config.contract.address).then(smartContract => {
    logger.info('contract instantiated at %s', config.contract.address);
    app.use('/api/v1', rest(smartContract));
}).catch(err => {
    logger.error('failed to instantiate contract %s', err);
    return;
});


module.exports = app;