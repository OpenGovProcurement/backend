//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);

describe('Purchase', () => {
    /*
      * Test the /GET route
      */

    const purchaseObject = {
        field: '',
        shortdescription: 'Закупка спортивной обуви',
        organisation: 'ООО "Баскетбольный клуб "Горизонт"',
        name: 'ООО "Баскетбольный клуб "Горизонт" Беларусь, Минский район, 223050, 9-й км. Московского шоссе, АБК ОДО "Виталюр", корпус 1, комната 219 191317046',
        unp: '',
        place: '',
        contact: '',
        purchaseprocess: '',
        posttime: '',
        endtime: '',
        estimatedprice: 1000,
        item:'',
        itemname: '',
        quantity: '',
        status: 0,
        itemsupplyplace:'',
        okrbcode:''
    };

    describe('POST /api/v1/purchase', () => {
        it('it should process new purchases', (done) => {
            chai.request(server)
                .post('/api/v1/purchase')
                .send(purchaseObject)
                .end((err, res) => {
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

});